import Weapon from "./Weapon"

export default class Hero {
    name: string
    power: number
    life: number
    weapon: Weapon

    constructor(name: string, power: number, life: number) {
        this.name = name
        this.power = power
        this.life = life
        this.weapon = Weapon
    }

    attack(opponent: Hero, damage: number) {
        // vérifier qu'il reste au moins 1 vie pour attaquer
        if (this.life === 0) {
            console.log(`${this.name} tu n'as plus de vie pour attaquer : GAME OVER.`);
        } else {
            console.log(`${this.name} attaque de ${damage}  sur ${opponent.name}`);
            // CE héros(l'attaquant) peut attaquer SI il a assez de power et de life pour le faire,  
            if (this.power >= damage) {
                // réduire 'power' de CE héros(l'attaquant) 
                // this.power -= damage;
                // réduire 'life' du héros attaqué
                opponent.life -= damage;
            }
            else {
                console.log(`${this.name}, tu n'a pas assez de power pour cette attaque.`);
            }
        }
    }

    isAlive(){
        // CE héros est en vie si il lui reste des 'life' 
        if (this.life > 0) {
           console.log(`${this.name}, il te reste ${this.life} vies.`);
           return true;
        } else {
            console.log(`${this.name}, tu n'as plus de vie : GAME OVER.`);
            return false;
        }
    }
}