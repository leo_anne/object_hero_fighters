import Hero from "./Hero";
import HeroSword from "./HeroSword";
import Weapon from "./Weapon";

export default class HeroAxe extends Hero {
    constructor(name: string, power: number, life: number) {
        super(name, power, life);

    }

    axe = new Weapon('axe');

    axeAttack(_opponent: Hero, _damage: number) {

        super.attack(_opponent, _damage);

        if (HeroSword) {
            _opponent.power -= _damage;
        }
    }
}