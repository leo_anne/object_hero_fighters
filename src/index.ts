import Hero from "./Hero";
import HeroAxe from "./HeroAxe";
import HeroSword from "./HeroSword";
import HeroSpear from "./HeroSpear";
import Weapon from "./Weapon";
import bataille from "./batailles";


const wonderwoman = new Hero("wonderwoman", 20, 10);
const starlight = new Hero('starlight', 10, 40);

console.log(wonderwoman);
console.log(starlight);

// test fonction isAlive')
console.log(wonderwoman.isAlive());
console.log(starlight.isAlive());

// tests fonction attack()
console.log('ATTAQUE');
wonderwoman.attack(starlight, 40);
console.log(wonderwoman);
console.log(starlight);
console.log('ATTAQUE');
starlight.attack(wonderwoman, 10);
console.log(wonderwoman);
console.log(starlight);
console.log('ATTAQUE');
wonderwoman.attack(starlight, 20);
console.log(wonderwoman);
console.log(starlight);

// Crée des instances des trois classes `HeroAxe`, `HeroSword` et `HeroSpear`
const pickaxe = new HeroAxe("pickaxe", 60, 50);
const pickaSpear = new HeroSpear("pickaSpear", 20, 30);
const pickaSword = new HeroSword('pickaSword', 20, 40);
console.log(pickaxe);
console.log(pickaSpear);
console.log(pickaSword);

// attaque  Spear vs Axe
console.log('ATTAQUE Spear vs Axe');
pickaSpear.spearAttack(pickaxe, 10);
console.log(pickaxe);
console.log(pickaSpear);
// attaque  Spear vs Hero
console.log('ATTAQUE Spear vs Hero');
pickaSpear.spearAttack(starlight, 10);
console.log(starlight);
console.log(pickaSpear);

// attaque  Axe vs Hero
console.log('ATTAQUE Axe vs Hero');
pickaxe.axeAttack(starlight, 5);
console.log(pickaxe);
console.log(starlight);
// attaque  Axe vs Sword
console.log('ATTAQUE Axe vs Sword');
pickaxe.axeAttack(pickaSword, 5);
console.log(pickaxe);
console.log(pickaSword);

// attaque  Sword vs Hero
console.log('ATTAQUE Sword vs Hero');
pickaSword.swordAttack(starlight, 3);
console.log(pickaSword);
console.log(starlight);

// attaque  Sword vs Spear
console.log('ATTAQUE Sword vs Spear');
pickaSword.swordAttack(pickaSpear, 6);
console.log(pickaSword);
console.log(pickaSpear);

 console.log(pickaxe);

 /****************** BATAILLES******************/ 
 bataille(pickaSpear, pickaSword);