import Hero from "./Hero";

export default function bataille(a:Hero, b:Hero){

    while (a.isAlive() || b.isAlive()){

        a.attack(b, b.power);
        b.attack(a, a.power);       
    }
    if(!a.isAlive()){
        console.log(`${b.name} wins`);       
    }else if(!b.isAlive()){
        console.log(`${a.name} wins`);
    }else{
        console.log(` It's a draw !`);        
    }
}
