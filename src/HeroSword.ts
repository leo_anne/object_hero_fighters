import Hero from "./Hero";
import HeroSpear from "./HeroSpear";
import Weapon from "./Weapon";

export default class HeroSword extends Hero {
    constructor(name: string, power: number, life: number) {
        super(name, power, life);

    }
    sword = new Weapon("sword");

    swordAttack(_opponent: Hero, _damage: number) {

        super.attack(_opponent, _damage);

        if (HeroSpear) {
            _opponent.power -= _damage;
        }
    }
}