import Hero from "./Hero";
import HeroAxe from "./HeroAxe";
import Weapon from "./Weapon";

export default class HeroSpear extends Hero {
    constructor(name: string, power: number, life: number) {
        super(name, power, life);
    }

    spear = new Weapon("spear");

    // HeroSpear` : si le type de `opponent` est `HeroAxe`, multiplier `damage` par deux
    // si heroSpear attaque HeroAxe alors damage sur HeroAxe *2
    spearAttack(_opponent: Hero,_damage: number) {
            
            super.attack(_opponent, _damage);
            if(HeroAxe){
                console.log('power x2');
                
                _opponent.power -= _damage*2;
            }
    }
}